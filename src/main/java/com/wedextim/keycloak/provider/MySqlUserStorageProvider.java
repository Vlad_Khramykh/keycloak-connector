package com.wedextim.keycloak.provider;

import com.wedextim.keycloak.models.User;
import com.wedextim.keycloak.repositories.UserRepo;
import com.wedextim.keycloak.representations.UserRepresentation;
import lombok.SneakyThrows;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.*;
import org.keycloak.models.cache.CachedUserModel;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MySqlUserStorageProvider implements UserStorageProvider,
        UserLookupProvider,
        UserRegistrationProvider,
        UserQueryProvider,
        CredentialInputUpdater,
        CredentialInputValidator {

    private final KeycloakSession session;
    private final ComponentModel model;
    private final UserRepo userRepo;

    Logger logger = Logger.getLogger(this.getClass().getName());

    public MySqlUserStorageProvider(KeycloakSession session, ComponentModel model, UserRepo userRepo) {
        this.session = session;
        this.model = model;
        this.userRepo = userRepo;
    }

    @Override
    public void close() {
        userRepo.close();
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        return supportsCredentialType(credentialType) && getPassword(user) != null;
    }

    @SneakyThrows
    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput credentialInput) {
        logger.info("isValid(" + realm + ", " + user.getId() + ", " + user.getEmail() + credentialInput + ")");
        if (!(credentialInput instanceof UserCredentialModel)) return false;
        if (supportsCredentialType(credentialInput.getType())) {
            UserCredentialModel cred = (UserCredentialModel) credentialInput;
            String password = getPassword(user);
            String hashedPass = hashPassword(cred.getValue());
            logger.info("isValid( password hash: " + hashedPass + " )");
            return password.equals(hashedPass);
        } else {
            return false;
        }
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @SneakyThrows
    @Override
    public boolean updateCredential(RealmModel realm, UserModel userModel, CredentialInput input) {
        logger.info("updateCredential(" + realm + ", " + userModel.getId() + ", " + input + ")");

        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) return false;
        String[] claims = userModel.getId().split(":");

        User user = new User();
        user.setEmail(userModel.getEmail());
        String password = input.getChallengeResponse();
        String hashedString = hashPassword(password);
        user.setPassword(hashedString);

        userRepo.updateUser(user);
        return true;
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel user, String credentialType) {
        if (!supportsCredentialType(credentialType)) return;
        getUserRepresentation(user).setPassword(null);
    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realm, UserModel user) {
        if (getUserRepresentation(user).getPassword() != null) {
            Set<String> set = new HashSet<>();
            set.add(PasswordCredentialModel.TYPE);
            return set;
        } else {
            return Collections.emptySet();
        }
    }

    public UserRepresentation getUserRepresentation(UserModel user) {
        UserRepresentation userRepresentation = null;
        if (user instanceof CachedUserModel) {
            userRepresentation = (UserRepresentation) ((CachedUserModel) user).getDelegateForUpdate();
        } else {
            userRepresentation = (UserRepresentation) user;
        }
        return userRepresentation;
    }

    public UserRepresentation getUserRepresentation(User user, RealmModel realm) {
        return new UserRepresentation(session, realm, model, user, userRepo);
    }

    @Override
    public int getUsersCount(RealmModel realm) {
        return userRepo.size();
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm) {
        return userRepo.findAll()
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user, userRepo))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
        return userRepo.findAll(firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user, userRepo))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm) {
        return userRepo.searchForUserByUsernameOrEmail(search)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user, userRepo))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
        return userRepo.searchForUserByUsernameOrEmail(search, firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user, userRepo))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
        // TODO Will probably never implement; Only used by REST API
        return new ArrayList<>();
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult,
                                         int maxResults) {
        return userRepo.findAll(firstResult, maxResults)
                .stream()
                .map(user -> new UserRepresentation(session, realm, model, user, userRepo))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        // TODO Will probably never implement
        return new ArrayList<>();
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
        // TODO Will probably never implement
        return new ArrayList<>();
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
        // TODO Will probably never implement
        return new ArrayList<>();
    }

    @Override
    public UserModel getUserById(String id, RealmModel realm) {
//         keycloakId := keycloak internal id; needs to be mapped to external id
//        String id = StorageId.externalId(keycloakId);
        User user = userRepo.getUserById(id);
        return new UserRepresentation(session, realm, model, user, userRepo);
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        Optional<User> optionalUser = userRepo.getUserByEmail(username);
        return getUserRepresentation(optionalUser.get(), realm);
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        Optional<User> optionalUser = userRepo.getUserByEmail(email);
        optionalUser.ifPresent(user -> logger.info(user.getEmail() + " - " + email));
        return getUserRepresentation(optionalUser.get(), realm);
    }

    @Override
    public UserModel addUser(RealmModel realm, String username) {
        logger.info("addUser");
        User user = new User();
        user = userRepo.createUser(user);

        return new UserRepresentation(session, realm, model, user, userRepo);
    }

    @Override
    public boolean removeUser(RealmModel realm, UserModel user) {
        User userEntity = userRepo.getUserById(StorageId.externalId(user.getEmail()));
        if (userEntity == null) {
            return false;
        }
        userRepo.deleteUser(userEntity);
        return true;
    }

    public String getPassword(UserModel user) {
        User foundUser = userRepo.getUserById(StorageId.externalId(user.getId()));
        return foundUser.getPassword();
    }

    public int getRole(UserModel user) {
        Optional<User> foundUser = userRepo.getUserByEmail(StorageId.externalId(user.getEmail()));
        return foundUser.get().getRole();
    }

    public static String hashPassword(String password) throws NoSuchAlgorithmException {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedHash = digest.digest(
                password.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexString = new StringBuilder(2 * encodedHash.length);
        for (int i = 0; i < encodedHash.length; i++) {
            String hex = Integer.toHexString(0xff & encodedHash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
