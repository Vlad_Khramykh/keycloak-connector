package com.wedextim.keycloak.representations;

import com.wedextim.keycloak.models.User;
import com.wedextim.keycloak.repositories.UserRepo;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserRepresentation extends AbstractUserAdapterFederatedStorage {
    private User userEntity;
    private UserRepo userRepo;
    private String keycloakId;

    public UserRepresentation(KeycloakSession session,
                              RealmModel realm,
                              ComponentModel storageProviderModel,
                              User userEntity,
                              UserRepo userRepo) {
        super(session, realm, storageProviderModel);
        this.userEntity = userEntity;
        this.keycloakId = StorageId.keycloakId(storageProviderModel, userEntity.getId());
        this.userRepo = userRepo;
    }

    @Override
    public String getUsername() {
        return userEntity.getEmail();
    }

    @Override
    public String getEmail() {
        return userEntity.getEmail();
    }

    @Override
    public String getId() {
        return this.keycloakId;
    }

    public String getPassword() {
        return userEntity.getPassword();
    }


    @Override
    public void setUsername(String username) {
    }

    @Override
    public void setEmail(String email) {
        userEntity.setEmail(email);
        userEntity = userRepo.updateUser(userEntity);
    }

    @Override
    public void setSingleAttribute(String name, String value) {
        super.setSingleAttribute(name, value);
    }

    @Override
    public void removeAttribute(String name) {
        super.removeAttribute(name);
        userEntity = userRepo.updateUser(userEntity);
    }

    @Override
    public void setAttribute(String name, List<String> values) {
        super.setAttribute(name, values);
        userEntity = userRepo.updateUser(userEntity);
    }

    @Override
    public String getFirstAttribute(String name) {
        return super.getFirstAttribute(name);
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        Map<String, List<String>> attrs = super.getAttributes();
        MultivaluedHashMap<String, String> all = new MultivaluedHashMap<>();
        all.putAll(attrs);
        return all;
    }

    @Override
    public List<String> getAttribute(String name) {
        if (name.equals("phone")) {
            List<String> phone = new LinkedList<>();
            return phone;
        } else {
            return super.getAttribute(name);
        }
    }





    public void setPassword(String password) {
        userEntity.setPassword(password);
        userEntity = userRepo.updateUser(userEntity);
    }
}
