package com.wedextim.keycloak.models;

import lombok.Data;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = "getUserByEmail", query = "select u from User u where u.email = :email"),
        @NamedQuery(name = "getUserById", query = "select u from User u where u.id = :id"),
        @NamedQuery(name = "getUserCount", query = "select count(u) from User u"),
        @NamedQuery(name = "getAllUsers", query = "select u from User u"),
        @NamedQuery(name = "searchForUser", query = "select u from User u where " +
                "( u.email like :search ) order by u.email"),
})
@Entity
@Table(name = "user")
@Data
public class User {
    @Id
    private String id;
    private String email;
    private String password;
    private int role;
}
