package com.wedextim.keycloak.repositories;

import com.wedextim.keycloak.models.User;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.StorageId;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

public class UserRepo {

    private final EntityManager entityManager;
    Logger logger = Logger.getLogger(UserRepo.class.getName());

    public UserRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<User> findAll() {
        return findAll(null, null);
    }

    public List<User> findAll(int start, int max) {
        return findAll((Integer) start, (Integer) max);
    }

    private List<User> findAll(Integer start, Integer max) {
        TypedQuery<User> query = entityManager.createNamedQuery("searchForUser", User.class);
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        query.setParameter("search", "%");
        List<User> resultList = query.getResultList();
        return resultList;
    }

    public Optional<User> getUserByEmail(String email) {
        TypedQuery<User> query = entityManager.createNamedQuery("getUserByEmail", User.class);
        query.setParameter("email", email);
        return query.getResultList().stream().findFirst();
    }

    public List<User> searchForUserByUsernameOrEmail(String searchString) {
        return searchForUserByUsernameOrEmail(searchString, null, null);
    }

    public List<User> searchForUserByUsernameOrEmail(String searchString, int start, int max) {
        return searchForUserByUsernameOrEmail(searchString, (Integer) start, (Integer) max);
    }

    private List<User> searchForUserByUsernameOrEmail(String searchString, Integer start, Integer max) {
        TypedQuery<User> query = entityManager.createNamedQuery("searchForUser", User.class);
        query.setParameter("search", "%" + searchString + "%");
        if (start != null) {
            query.setFirstResult(start);
        }
        if (max != null) {
            query.setMaxResults(max);
        }
        return query.getResultList();
    }

    public User getUserById(String id) {
        logger.info("getUserById: " + id);
        String persistenceId = StorageId.externalId(id);
        logger.info("getUserByPersistenceId: " + persistenceId);
        User entity = entityManager.find(User.class, persistenceId);
        if (entity == null) {
            logger.info("could not find user by id: " + id);
            return null;
        }
        return entity;
    }

    public User createUser(User user) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        return user;
    }

    public void deleteUser(User user) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(user);
        transaction.commit();
    }

    public void close() {
        this.entityManager.close();
    }

    public User updateUser(User userEntity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(userEntity);
        transaction.commit();
        return userEntity;
    }

    public int size() {
        return entityManager.createNamedQuery("getUserCount", Integer.class).getSingleResult();
    }
}
